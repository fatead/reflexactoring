/**
 */
package reflexactoring.impl;

import org.eclipse.emf.ecore.EClass;

import reflexactoring.ModuleExtend;
import reflexactoring.ReflexactoringPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Module Extend</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ModuleExtendImpl extends ModuleLinkImpl implements ModuleExtend {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModuleExtendImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ReflexactoringPackage.Literals.MODULE_EXTEND;
	}

} //ModuleExtendImpl
