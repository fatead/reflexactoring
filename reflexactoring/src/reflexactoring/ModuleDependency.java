/**
 */
package reflexactoring;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see reflexactoring.ReflexactoringPackage#getModuleDependency()
 * @model
 * @generated
 */
public interface ModuleDependency extends ModuleLink {

} // ModuleDependency
