/**
 */
package reflexactoring;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Creation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see reflexactoring.ReflexactoringPackage#getModuleCreation()
 * @model
 * @generated
 */
public interface ModuleCreation extends ModuleLink {
} // ModuleCreation
