/**
 */
package reflexactoring;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Module Extend</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see reflexactoring.ReflexactoringPackage#getModuleExtend()
 * @model
 * @generated
 */
public interface ModuleExtend extends ModuleLink {
} // ModuleExtend
