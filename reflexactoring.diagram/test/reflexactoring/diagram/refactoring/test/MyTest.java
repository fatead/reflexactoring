/**
 * 
 */
package reflexactoring.diagram.refactoring.test;

import org.eclipse.swt.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.*;

/**
 * @author fdse
 *
 */
public class MyTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		  Display display = new Display();
	        Shell shell = new Shell(display);
	        

	        //Scale
	        final Scale scale = new Scale(shell, SWT.HORIZONTAL);
	        scale.setBounds(10, 50, 200, 72);
	        scale.setMinimum(0);
	        scale.setMaximum(100);
	        scale.addSelectionListener(new SelectionAdapter() {
	            public void widgetSelected(SelectionEvent e) {
	                System.out.println("Scale Selection:" + scale.getSelection()*0.01);
	            }
	        });
	        
	        shell.open();
	        while (!shell.isDisposed()) {
	            if (!display.readAndDispatch())
	                display.sleep();
	        }
	        display.dispose();

	}

}
