/**
 * 
 */
package datamining.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * @author linyun
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ TestHierarchicalClustering.class })
public class AllTests {
	
}
