/**
 * 
 */
package reflexactoring.diagram.util;

import java.util.ArrayList;
import java.util.HashMap;

import reflexactoring.diagram.action.smelldetection.refactoringopportunities.RefactoringOpportunity;
import reflexactoring.diagram.bean.DependencyWrapper;
import reflexactoring.diagram.bean.ModuleWrapper;
import reflexactoring.diagram.bean.efficiency.UnitPair;
import reflexactoring.diagram.bean.heuristics.HeuristicModuleMemberStopMapList;
import reflexactoring.diagram.bean.heuristics.HeuristicModulePartFixMemberMapList;
import reflexactoring.diagram.bean.heuristics.HeuristicModuleUnitFixMemberMapList;
import reflexactoring.diagram.bean.heuristics.HeuristicModuleUnitMapList;
import reflexactoring.diagram.bean.heuristics.HeuristicModuleUnitStopMapList;
import reflexactoring.diagram.bean.heuristics.ModuleCreationConfidenceTable;
import reflexactoring.diagram.bean.heuristics.ModuleDependencyConfidenceTable;
import reflexactoring.diagram.bean.heuristics.ModuleExtendConfidenceTable;
import reflexactoring.diagram.bean.heuristics.ModuleUnitsSimilarityTable;
import reflexactoring.diagram.bean.programmodel.ProgramModel;

/**
 * @author linyun
 *
 */
public class Settings {

	/**
	 * Temporarily used for global variable of user-selected 
	 * java classes/interfaces.
	 */
	public static ProgramModel scope = new ProgramModel();
	
	/**
	 * Weight Val for scale 
	 */
	public static double CBO_Weight=0.0;
	public static double LCOM_Weight=0.0;
	public static double NOM_Weight=0.0;
	public static double SALF_Weight=0.0;
	
	/**
	 * 三个指标的值
	 * */
	public static double CBO=0.0;
	public static double LCOM=0.0;
	public static double NOM=0.0;
	public static double SALF=0.0;
	
	/**
	 * 是否使用高层模型
	 * */
	public static boolean NeedHighLevelModel=false;
	public static boolean LCOM_STATIC=false;
	public static boolean CBO_STATIC=false;
	public static boolean NOM_STATIC=false;
	
	/**
	 * It is used for specifying user rejected refactoring suggestions, which could used as hints to add penalty
	 * to similar refactoring suggestions.
	 */
	public static ArrayList<RefactoringOpportunity> forbiddenOpps = new ArrayList<>();
	
	
	/**
	 * It is used for specifying user approved refactoring suggestions, which could be used as hints to add reward
	 * to similar refactoring suggestions.
	 */
	public static ArrayList<RefactoringOpportunity> approvedOpps = new ArrayList<>();
	/**
	 * It is used for keeping user-specified module-type(unit) mapping relation.
	 */
	public static HeuristicModuleUnitMapList heuristicModuleUnitFixList
		= new HeuristicModuleUnitMapList();
	
	/**
	 * It is used for keeping user-specified forbidden list indicating which type(unit) can never
	 * be mapped to which module.
	 */
	public static HeuristicModuleUnitStopMapList heuristicModuleUnitStopMapList
		= new HeuristicModuleUnitStopMapList();
	
	/**
	 * It is used for keeping user-specified forbidden list indicating which member can never
	 * be mapped to which module.
	 */
	public static HeuristicModuleMemberStopMapList heuristicModuleMemberStopMapList
		= new HeuristicModuleMemberStopMapList();
	
	/**
	 * It is used for keeping user-defined fixed list. In other words, when user fix a module
	 * and type, he/she force that all the members of a type can only be mapped to the module.
	 */
	public static HeuristicModuleUnitFixMemberMapList heuristicModuleUnitMemberFixList
		= new HeuristicModuleUnitFixMemberMapList();
	
	/**
	 * It is used for keeping user-defined fixed list. In other words, when user fix part a module
	 * and type, he/she force that part the members of a type can only be mapped to the module.
	 */
	public static HeuristicModulePartFixMemberMapList heuristicModuleMemberPartFixList
		= new HeuristicModulePartFixMemberMapList();
	
	/**
	 * It is used to keep user-specified module-type similarity value. For each pair
	 * of module and type, there is a similarity between them.
	 */
	public static ModuleUnitsSimilarityTable similarityTable = new ModuleUnitsSimilarityTable();
	
	/**
	 * It is used to record module-module dependency confidence. If the confidence is low, e.g., 0.1,
	 * it means user is not quite confident in such a dependency constraint, which will lead 
	 * my algorithm considering tolerating some violation.
	 */
	public static ModuleDependencyConfidenceTable dependencyConfidenceTable = new ModuleDependencyConfidenceTable();
	
	/**
	 * It is used to record module-module extend confidence. If the confidence is low, e.g., 0.1,
	 * it means user is not quite confident in such a extend constraint, which will lead 
	 * my algorithm considering tolerating some violation.
	 */
	public static ModuleExtendConfidenceTable extendConfidenceTable = new ModuleExtendConfidenceTable();
	
	/**
	 * It is used to record module-module creation confidence. If the confidence is low, e.g., 0.1,
	 * it means user is not quite confident in such a creation constraint, which will lead 
	 * my algorithm considering tolerating some violation.
	 */
	public static ModuleCreationConfidenceTable creationConfidenceTable = new ModuleCreationConfidenceTable();
	
	/**
	 * If a module is frozen on diagram, no member will be recommended to move into it, nor will any member
	 * inside it will be move out of it.
	 */
	public static ArrayList<ModuleWrapper> frozenModules = new ArrayList<>();
	
	public static boolean isSkipUnMappedTypes = false;
	public static boolean isCompliationUnitChanged = true;
	public static boolean isNeedClearCache = true;
		
	public static String diagramPath = "/refactoring/default.reflexactoring";
	public static double largeSimilarityValue = 1.0d;
	
	/**
	 * This constants is used to improve the efficiency of computing similarity.
	 */
	public static HashMap<UnitPair, Double> unitPairSimilarityMap = new HashMap<>();
	
	//=============================================================
	/**
	 * The following is the parameters for optimization
	 */
	public static double alpha = 0.5;
	public static double beta = 0.5;
	public static double mutationRate = 0.01;
	public static int mappingIterationNum = 500;
	public static int climbIterationNum = 20;
	public static int populationSize = 100;
	public static int suggestionNum = 3;
	
	public static double emptyModulePenalty = 10;
	
	//=============================================================
	/**
	 * The following is the parameters for k-step optimization
	 */
	public static int kStep = 3;
	public static int bucketSize = 10;
	
	//==============================================================
	/**
	 * The following is the parameters for detecting bad smell
	 */
	public static final double counterMethodSimilarity = 0.8;
	public static final double featureEnvyThreshold = 0.05;
	
	public static final double penaltyRate = 0.2;
	public static final double rewardRate = 0.2;
	
	public static final double refactoringOppSimilarity = 0.6;

	public static boolean isScopeRefreshed = true;
	
	//===============================================================
	/**
	 * The following is used to indicate the visibility of lines on graph
	 */
	public static boolean enableVisibility = false;
	/**
	 * indicate those type dependencies need to be highlighted
	 */
	public static ArrayList<DependencyWrapper> highlightLinks = new ArrayList<>();
}
