/**
 * 
 */
package reflexactoring.diagram.bean.programmodel;

/**
 * @author linyun
 *
 */
public class GraphRelationType {
	/**
	 * Graph types 
	 */
	public static final int GRAPH_DEPENDENCY = 1;
	public static final int GRAPH_INHERITANCE = 2;
	public static final int GRAPH_CREATION = 3;
}
