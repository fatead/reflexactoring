/**
 * 
 */
package reflexactoring.diagram.bean.programmodel;

/**
 * @author linyun
 *
 */
public interface SimilarityComputable {
	public double computeSimilarityWith(Object obj);
}
