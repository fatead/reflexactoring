/**
 * 
 */
package reflexactoring.diagram.view;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import reflexactoring.diagram.util.Settings;


/**
 * @author Hu_codeman
 *
 */
public class ScaleView extends ViewPart {

	public static final String ID = "reflexactoring.diagram.view.ScaleView"; //$NON-NLS-1$
	
	
	private Scale scale_cbq;
	private Scale scale_lcom;
	private Scale scale_tbq;
	private Scale scale_salf;
	//Test Data
	
	private double temp1;
	private double temp2;
	private double temp3;
	private double temp4;
	

	public ScaleView() {
		
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		
		scale_cbq = new Scale(container, SWT.NONE);
		//scale_cbq.setMinimum(1);
		scale_cbq.setBounds(179, 74, 217, 42);
		scale_cbq.setEnabled(false);
		
		scale_lcom = new Scale(container, SWT.NONE);
		scale_lcom.setBounds(179, 147, 217, 42);
		scale_lcom.setEnabled(false);
		
		scale_tbq = new Scale(container, SWT.NONE);
		scale_tbq.setBounds(179, 218, 217, 42);
		scale_tbq.setEnabled(false);
		
		scale_salf = new Scale(container, SWT.NONE);
		scale_salf.setBounds(179, 296, 217, 42);
		scale_salf.setEnabled(false);
		
		Label lblLcom = new Label(container, SWT.NONE);
		lblLcom.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9, SWT.BOLD | SWT.ITALIC));
		lblLcom.setBounds(112, 160, 61, 17);
		lblLcom.setText("  LCOM");
		
		Label lblCbq = new Label(container, SWT.NONE);
		lblCbq.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9, SWT.BOLD | SWT.ITALIC));
		lblCbq.setBounds(112, 87, 61, 17);
		lblCbq.setText("   CBO");
		
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9, SWT.BOLD | SWT.ITALIC));
		lblNewLabel.setBounds(112, 231, 61, 17);
		lblNewLabel.setText("     NOM");
		
		Label lblDbq = new Label(container, SWT.NONE);
		lblDbq.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 9, SWT.BOLD | SWT.ITALIC));
		lblDbq.setBounds(112, 308, 61, 17);
		lblDbq.setText("     SALF");
		
		Label CBO_Val = new Label(container, SWT.NONE);
		CBO_Val.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD | SWT.ITALIC));
		CBO_Val.setBounds(436, 74, 61, 26);
		//lblNewLabel_1.setFont(SWTResourceManager.getFont(name, height, style));
		CBO_Val.setText("0");
		
		Label LCOM_Val = new Label(container, SWT.NONE);
		LCOM_Val.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD | SWT.ITALIC));
		LCOM_Val.setText("0");
		LCOM_Val.setBounds(436, 147, 61, 26);
		
		Label NOM_Val = new Label(container, SWT.NONE);
		NOM_Val.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD | SWT.ITALIC));
		NOM_Val.setText("0");
		NOM_Val.setBounds(436, 223, 61, 26);
		
		Label SALF_Val = new Label(container, SWT.NONE);
		SALF_Val.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD | SWT.ITALIC));
		SALF_Val.setBounds(436, 306, 61, 26);
		SALF_Val.setText("0");
		
		Button CBO_CheckButton = new Button(container, SWT.CHECK);
		CBO_CheckButton.setBounds(81, 87, 13, 17);
		
		Button LCOM_CheckButton = new Button(container, SWT.CHECK);
		LCOM_CheckButton.setBounds(81, 160, 13, 17);
		
		Button NOM_CheckButton = new Button(container, SWT.CHECK);
		NOM_CheckButton.setBounds(81, 233, 13, 17);
		
		Button SALF_CheckButton = new Button(container, SWT.CHECK);
		SALF_CheckButton.setBounds(81, 306, 13, 17);
		
		CBO_CheckButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {  
				if(!CBO_CheckButton.getSelection()) {
					scale_cbq.setEnabled(false);
					Settings.CBO_STATIC = false;
					temp1 = Settings.CBO_Weight;
					Settings.CBO_Weight = 0.00;
					CBO_CheckButton.setSelection(false);  
		        } else {  
		        	scale_cbq.setEnabled(true);
		        	Settings.CBO_STATIC = true;
		        	Settings.CBO_Weight = temp1;
		        	CBO_CheckButton.setSelection(true);  
		        }  
            }
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//scale_cbq.setEnabled(true);
			}  
		});
		
		LCOM_CheckButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {  
				if(!LCOM_CheckButton.getSelection()) {
					scale_lcom.setEnabled(false);
					Settings.LCOM_STATIC = false;
					temp2 = Settings.LCOM_Weight;
					Settings.LCOM_Weight = 0.00;
					LCOM_CheckButton.setSelection(false);  
		        } else {  
		        	scale_lcom.setEnabled(true);
		        	Settings.LCOM_STATIC = true;
		        	Settings.LCOM_Weight = temp2;
		        	LCOM_CheckButton.setSelection(true);  
		        }  
            }
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//scale_cbq.setEnabled(true);
			}  
		});
		NOM_CheckButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {  
				if(!NOM_CheckButton.getSelection()) {
					scale_tbq.setEnabled(false);
					Settings.NOM_STATIC = false;
					temp3 = Settings.NOM_Weight;
					Settings.NOM_Weight = 0.00;
					NOM_CheckButton.setSelection(false);  
		        } else {  
		        	scale_tbq.setEnabled(true);
		        	Settings.NOM_STATIC = true;
		        	Settings.NOM_Weight = temp3;
		        	NOM_CheckButton.setSelection(true);  
		        }  
            }
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//scale_cbq.setEnabled(true);
			}  
		});
		SALF_CheckButton.addSelectionListener(new SelectionListener() {
			public void widgetSelected(SelectionEvent e) {  
				if(!SALF_CheckButton.getSelection()) {
					scale_salf.setEnabled(false);
					Settings.NeedHighLevelModel = false;
					temp4 = Settings.SALF_Weight;
					Settings.SALF_Weight = 0.00;
					SALF_CheckButton.setSelection(false);  
		        } else {  
		        	scale_salf.setEnabled(true);
		        	Settings.NeedHighLevelModel = true;
		        	Settings.SALF_Weight = temp4;
		        	SALF_CheckButton.setSelection(true);  
		        }  
            }
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				//scale_cbq.setEnabled(true);
			}  
		});

		scale_cbq.addListener(SWT.Selection, new Listener() {
		      public void handleEvent(Event event) {
		    	  Settings.CBO_Weight = scale_cbq.getSelection() /100.0;
		          //value.setText("Vol: " + perspectiveValue);
		  		  CBO_Val.setText("" + Settings.CBO_Weight);
		      }
		    });
		scale_lcom.addListener(SWT.Selection, new Listener() {
		      public void handleEvent(Event event) {
		    	  Settings.LCOM_Weight = scale_lcom.getSelection() /100.0;
		          //value.setText("Vol: " + perspectiveValue);
		    	  LCOM_Val.setText("" + Settings.LCOM_Weight);
		      }
		    });
		scale_tbq.addListener(SWT.Selection, new Listener() {
		      public void handleEvent(Event event) {
		    	  Settings.NOM_Weight = scale_tbq.getSelection() /100.0;
		          //value.setText("Vol: " + perspectiveValue);
		  		  NOM_Val.setText("" + Settings.NOM_Weight);
		      }
		    });
		
		scale_salf.addListener(SWT.Selection, new Listener() {
		      public void handleEvent(Event event) {
					Settings.SALF_Weight= scale_salf.getSelection() /100.0;
					SALF_Val.setText("" + Settings.SALF_Weight);
		      }
		    });
		
		
		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
		

	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}
}
