/**
 * 
 */
package reflexactoring.diagram.view;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.Canvas;

import java.awt.Color;
import java.awt.Component;
import javax.swing.Box;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.Transform;
import org.eclipse.swt.graphics.Drawable;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.PaintEvent;
import java.lang.Math;
import java.text.DecimalFormat;

import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import reflexactoring.diagram.util.Settings;
//import org.eclipse.swt.graphics.Color;
/**
 * @author Hu_codeman
 *
 */
public class ValueFigureView extends ViewPart {

	public static final String ID = "reflexactoring.diagram.view.ValueFigureView"; //$NON-NLS-1$
	//private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	public double Arr[] = {Settings.CBO,Settings.LCOM,Settings.NOM,Settings.SALF};
	
	/**
	 * test data
	 */
//	public static final double one = 0.5;
//	public static final double two = 0.6;
//	public static final double three = 0.7;
	
	public int one_y;
	public int one_x;
	public int two_y;
	public int two_x;
	public int three_y;
	public int three_x;
	public int four_x;
	public int four_y;
	
	private Label lbl_CBO;
	private Label lbl_LCOM;
	private Label lbl_NOM;
	private Label lbl_SALF;
	
	private Canvas canvas;
	private Label lblCbo;
	private Label lblLcom;
	private Label lblNom;
	private Label lblSalf;
	
	public ValueFigureView() {
	}

	/**
	 * Create contents of the view part.
	 * @param parent
	 */
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		
		Canvas canvas = new Canvas(container, SWT.NONE);
		canvas.setBackground(SWTResourceManager.getColor(SWT.COLOR_GRAY));
		canvas.setBounds(10, 46, 574, 410);
		
		lblCbo = new Label(canvas, SWT.NONE);
		lblCbo.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD));
		lblCbo.setBounds(301, 22, 61, 30);
		lblCbo.setText("CBO");
		lblLcom = new Label(canvas, SWT.NONE);
		lblLcom.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD));
		lblLcom.setBounds(480, 170, 61, 30);
		lblLcom.setText("LCOM");
		
		lblNom = new Label(canvas, SWT.NONE);
		lblNom.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD));
		lblNom.setBounds(50, 170, 61, 30);
		lblNom.setText("NOM");
		
		lblSalf = new Label(canvas, SWT.NONE);
		lblSalf.setText("SALF");
		lblSalf.setFont(SWTResourceManager.getFont("Microsoft YaHei UI", 14, SWT.BOLD));
		lblSalf.setBounds(301, 355, 61, 30);
		
		lbl_CBO = new Label(canvas, SWT.NONE);
		lbl_LCOM = new Label(canvas, SWT.NONE);
		lbl_NOM = new Label(canvas, SWT.NONE);
		lbl_SALF = new Label(canvas, SWT.NONE);
		
		DecimalFormat df = new DecimalFormat("######0.00");  
		
		canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				//draw coordinate axis
				Rectangle clientArea = canvas.getClientArea(); 
		        //e.gc.drawLine(0,0,clientArea.width,clientArea.height);
				
				int origin_x = clientArea.width/2 ;
				int origin_y = clientArea.height/2;
				//int min_y = canvas.getBounds().y;
				int bias_x = (int) (Math.sqrt(3)*100);
				int bias_y = 100 ;
				e.gc.setLineWidth(4);
				e.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLUE));
				//e.gc.drawLine(origin_x,origin_y,origin_x+clientArea.width/2,origin_y);
				e.gc.drawLine(origin_x,origin_y,origin_x,origin_y - 200);
				//e.gc.drawLine()
				e.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
				e.gc.drawLine(origin_x,origin_y,origin_x + 200 ,origin_y);
				e.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_YELLOW));
				
				e.gc.drawLine(origin_x,origin_y,origin_x - 200,origin_y);
				e.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
				e.gc.drawLine(origin_x,origin_y,origin_x ,origin_y + 200);
				e.gc.setForeground(SWTResourceManager.getColor(SWT.COLOR_BLACK));
				//trsanform the location of point 
				if(Settings.CBO<=0.25&&Settings.LCOM<=0.4&&Settings.NOM<=10) {
					one_x = clientArea.width/2;
					one_y = (int)(clientArea.height/2 - Settings.CBO*200.0*4.0);
					two_x = (int)(clientArea.width/2 + Settings.LCOM*200.0*2.5);
					two_y = (int)(clientArea.height/2);
					three_x = (int)(clientArea.width/2 - Settings.NOM/10.0*200.0);
					three_y = (int)(clientArea.height/2);
					four_x = (int)(clientArea.width/2);
					four_y = (int)(clientArea.height/2 + Settings.SALF/50.0*200.0);
					
				}else {
					one_x = clientArea.width/2;
					one_y = (int)(clientArea.height/2 - Settings.CBO*200.0);
					two_x = (int)(clientArea.width/2 + Settings.LCOM*200.0);
					two_y = (int)(clientArea.height/2);
					three_x = (int)(clientArea.width/2 - Settings.NOM/50.0*200.0);
					three_y = (int)(clientArea.height/2);
					four_x = (int)(clientArea.width/2);
					four_y = (int)(clientArea.height/2 + Settings.SALF/50.0*200.0);
				}
				//show the value of variables
				 if(Settings.CBO !=0) {
						lbl_CBO.setBounds(one_x - 67, one_y - 12, 61, 17);
						lbl_CBO.setText("   " + df.format(Settings.CBO));
						lbl_CBO.setFont(SWTResourceManager.getFont("", 0, 30));}else{
							lbl_CBO.setText("   ");
						}
						
					    if(Settings.LCOM !=0) {
						lbl_LCOM.setBounds(two_x + 6, two_y - 20, 61, 17);
						lbl_LCOM.setText("" + df.format(Settings.LCOM));
						lbl_LCOM.setFont(SWTResourceManager.getFont("", 0, 30));}else{
							lbl_LCOM.setText(" ");
						}
						
					    if(Settings.NOM !=0) {
						lbl_NOM.setBounds(three_x - 67, three_y - 20, 61, 17);
						lbl_NOM.setText("  " + df.format(Settings.NOM));
						lbl_NOM.setFont(SWTResourceManager.getFont("", 0, 30));}else{
							lbl_NOM.setText(" ");
						}
						
					    if(Settings.SALF !=0&&Settings.NeedHighLevelModel&&Settings.SALF_Weight!=0) {
					    	
						lbl_SALF.setBounds(four_x - 67, four_y + 6, 61, 17);
						lbl_SALF.setText("   " + df.format(Settings.SALF));
						lbl_SALF.setFont(SWTResourceManager.getFont("", 0, 30));}else{
							lbl_SALF.setText(" " );
						}
						//draw point
						e.gc.drawPoint(one_x, one_y);
						e.gc.drawPoint(two_x, two_y);
						e.gc.drawPoint(three_x, three_y);
						e.gc.drawPoint(four_x, four_y);
						//draw line
						if(Count()==2)
						{
							if(Settings.CBO!=0&&Settings.LCOM!=0) e.gc.drawLine(one_x, one_y, two_x, two_y);
							if(Settings.CBO!=0&&Settings.NOM!=0) e.gc.drawLine(one_x, one_y, three_x, three_y);
							if(Settings.CBO!=0&&Settings.SALF!=0) e.gc.drawLine(one_x, one_y, four_x, four_y);
							if(Settings.LCOM!=0&&Settings.NOM!=0)  e.gc.drawLine(two_x, two_y, three_x, three_y);
							if(Settings.LCOM!=0&&Settings.SALF!=0) e.gc.drawLine(two_x, two_y, four_x, four_y);
							if(Settings.NOM!=0&&Settings.SALF!=0) e.gc.drawLine(three_x, three_y,four_x,four_y);
						} else {
						    e.gc.drawLine(one_x, one_y, two_x, two_y);
						    e.gc.drawLine(one_x, one_y, three_x, three_y);
						//e.gc.drawLine(two_x, two_y, three_x, three_y);
						    e.gc.drawLine(two_x, two_y, four_x, four_y);
						    e.gc.drawLine(three_x, three_y,four_x,four_y);
						}
			
			}
		});
		createActions();
		initializeToolBar();
		initializeMenu();
	}
	/**
	 * trsanform the location of point
	 */
	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		IToolBarManager toolbarManager = getViewSite().getActionBars().getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}
	
	public int Count() {
		int count = 0;
		for(int i=0;i<4;i++)
			if(Arr[i]!=0) count++;
		return count;
	}
}
