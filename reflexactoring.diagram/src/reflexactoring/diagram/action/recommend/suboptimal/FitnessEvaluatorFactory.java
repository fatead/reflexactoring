/**
 * 
 */
package reflexactoring.diagram.action.recommend.suboptimal;

import java.util.ArrayList;

import reflexactoring.diagram.action.ModelMapper;
import reflexactoring.diagram.action.recommend.RecommendUtil;
import reflexactoring.diagram.bean.ModuleWrapper;
import reflexactoring.diagram.bean.programmodel.GraphRelationType;
import reflexactoring.diagram.bean.programmodel.ProgramModel;
import reflexactoring.diagram.bean.programmodel.ReferencingDetail;

/**
 * @author linyun
 *
 */
public class FitnessEvaluatorFactory {
	
	public static FitnessEvaluator createFitnessEvaluator(ProgramModel model, ArrayList<ModuleWrapper> moduleList, int type){
		
		double[][] similarityTable = ModelMapper.initializeOverallSimilarityTable(moduleList, model.getOutmostTypesInScope());
		//double[][] similarityTable = Settings.similarityTable.convertModuleUnitsSimilarityTableToRawTable();
		System.currentTimeMillis();
		
		double[][] highLevelNodeDependencyMatrix = RecommendUtil.extractGraph(moduleList, 
				GraphRelationType.GRAPH_DEPENDENCY, ReferencingDetail.REFER);
		double[][] highLevelNodeInheritanceMatrix = RecommendUtil.extractGraph(moduleList, 
				GraphRelationType.GRAPH_INHERITANCE, ReferencingDetail.ALL);
		double[][] highLevelNodeCreationMatrix = RecommendUtil.extractGraph(moduleList, 
				GraphRelationType.GRAPH_CREATION, ReferencingDetail.NEW);
		
		double[][] lowLevelNodeDependencyMatrix = RecommendUtil.extractGraph(model.getOutmostTypesInScope(), 
				GraphRelationType.GRAPH_DEPENDENCY, ReferencingDetail.REFER);
		double[][] lowLevelNodeInheritanceMatrix = RecommendUtil.extractGraph(model.getOutmostTypesInScope(), 
				GraphRelationType.GRAPH_INHERITANCE, ReferencingDetail.ALL);
		double[][] lowLevelNodeCreationMatrix = RecommendUtil.extractGraph(model.getOutmostTypesInScope(), 
				GraphRelationType.GRAPH_CREATION, ReferencingDetail.NEW);
		
		switch(type){
		case FitnessEvaluator.ADVANCED_EVALUATOR:
			return new AdvancedFitnessEvaluator(similarityTable, highLevelNodeDependencyMatrix, lowLevelNodeDependencyMatrix, 
					highLevelNodeInheritanceMatrix, lowLevelNodeInheritanceMatrix, highLevelNodeCreationMatrix, 
					lowLevelNodeCreationMatrix);
		case FitnessEvaluator.BALANCED_EVALUATOR:
			return new BalancedFitnessEvaulator(similarityTable, highLevelNodeDependencyMatrix, lowLevelNodeDependencyMatrix, 
					highLevelNodeInheritanceMatrix, lowLevelNodeInheritanceMatrix, highLevelNodeCreationMatrix, 
					lowLevelNodeCreationMatrix);
		}
		
		return null;
	}
}
